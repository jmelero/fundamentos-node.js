// Nos traemos el módulo Filesystem

const fs = require('fs');
const { dirname } = require('path');

// Leer el fichero

function leer(ruta, cb) {
    fs.readFile(ruta, (error, data) => {
        console.log(data.toString());
    })
}

function escribir(ruta, contenido, cb) {
    fs.writeFile(ruta, contenido, function (error) {
        if (error) {
            console.error("No se ha podido escribir", error);
        }
        else{
            console.log("Se ha escrito correctamente");
        }
    });
}

function borrar(ruta, cb){
    fs.unlink(ruta, cb);
}

leer(__dirname + '/archivo.txt');

escribir(__dirname + '/escibir.txt', "Estoy escribiendo.", console.log());

borrar(__dirname + '/escibir.txt', console.log);
