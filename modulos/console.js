console.log('Algo');
console.info('Te informo de algo');
console.error();
console.warn('Cuidado!'); 


//En forma de módulos o tablas
console.table();

var tabla = [
    {
        a:1,
        b: 'Z',
    },
    {
        a:2,
        b:'Otra',
    }
];

console.table(tabla);

///Agrupa logs para decir que son grupo de una cosa
console.group('conver');
console.log('Hola');
console.log('Blalala');
console.log('Blalala');
console.log('Adios');
console.groupEnd('conv');


//Tiempo
console.time('tiempo');
console.timeEnd('tiempo');

//Conteo
console.count('veces');
console.count('veces');