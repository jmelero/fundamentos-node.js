// De forma 'vieja'
//const process = require('process');

// No hace falta process para el proceso actual
process.on('beforeExit', function () {
    console.log('Va a terminar');
})
process.on('exit', function () {
     console.log('Ale, el proceso acabó');
})

// Cuando no se captura un error

//Permite que no se rompa el programa
process.on('uncaughtException', (err, orign) => {
    console.error(err);
});

funcionQueNoExiste();

