// En PowerShell las variables de entorno se declaran como $env:NOMBRE="nombre"

let nombre = process.env.NOMBRE || "Sin nombre";

console.log('Hola ' + nombre);
console.log(`Hola ${nombre}`);