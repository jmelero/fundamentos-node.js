async function hola(nombre) {
    return new Promise(function(resolve,reject) {
        setTimeout( () => {
            console.log('Hola ' + nombre);
            resolve(nombre);
        }, 1000);
    });
}

async function adios(nombre) {
    return new Promise( function(resolve,reject) {
        setTimeout(() => {
            console.log('Adios ' + nombre);
            reject(nombre);            
        },1000)
    });
}

async function main() {
    try {
        let nombre = await hola('Pepe');    
        let nom = await adios('Juan');    
    } catch (error) {
        console.error(error);
    }
    
}

main();