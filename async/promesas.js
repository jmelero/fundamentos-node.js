function hola(nombre) {
    return new Promise(function(resolve,reject) {
        setTimeout( () => {
            console.log('Hola ' + nombre);
            resolve(nombre);
        }, 1000);
    });
}

function adios(nombre) {
    return new Promise( function(resolve,reject) {
        setTimeout(() => {
            console.log('Adios ' + nombre);
            otroCallback();
        },1000)
    });
}



hola('Carlos')
    .then( (nombre) => {
        console.log('Terminado el proceso ' + nombre);
    });