function hola(nombre, micallback) {
    console.log("Hola soy una función asíncrona");
    setTimeout( () => {
        console.log('Hola ' + nombre);
        micallback();
    }, 1000);
}

function adios(nombre,otroCallback) {
    setTimeout(() => {
        console.log('Adios ' + nombre);
        otroCallback();
    },1000)
}

console.log('Iniciando');
hola('Juan',function () {
    adios('Pepe', function () {
    console.log('Terminando proceso');    
    });
    
});
