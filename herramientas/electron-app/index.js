const { app, BrowserWindow } = require('electron');

// Ventana principal

let mainWindow;


// Como el BrowserWIndow se carga antes que la app, hay que esperar a que la app esté lista, y después cargar la creación de la ventana
app.on('ready', createWindow);

function createWindow() {
    mainWindow = new BrowserWindow( {
        width: 800,
        height: 600,
    });

    mainWindow.loadFile('./index.html');
}

