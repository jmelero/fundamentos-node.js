let suma = 0;
console.time('suma');
for ( let i=0; i < 1000000000; i++) {
    suma += 1;
}
console.timeEnd('suma');


let suma2 = 0;
console.time('suma2');
for ( let j=0; j < 1000000000; j++) {
    suma2 += 1;
}
console.timeEnd('suma2');


// Con funciones asíncronas
console.time('async');
function asincrona() {
    return new Promise ( (resolve) => {
        setTimeout(function() {
            console.log('Termina el proceso asíncrono');
            resolve();
        });
    })
}

asincrona()
.then( () => {
    console.timeEnd('async');
});

