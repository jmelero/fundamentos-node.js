console.log("Hola Mundo!");


// De esta forma Node sigue en marcha, ya que el EventLoop sigue en marcha y es asíncrona
let i = 0;
setInterval( () => {
    console.log(i);
    i++;   
}, 1000);


// De forma asíncrona

console.log("Segunda instrucción");